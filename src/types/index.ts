export interface ItemType {
  vote: number
  title: string
  link: string
  id: string
}
