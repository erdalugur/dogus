import { List } from './modules/list'
import { Add } from './modules/add'
import { CacheService, generateId } from './lib'
import { CACHE_KEYS } from './constants'
import './styles/index.scss'

const initalData = CacheService.get(CACHE_KEYS.DATA, [
  {
    "id": generateId(),"vote": 6, "title": "Hacker News", "link": "https://test-streams.mux.dev/x36xhzz/x36xhzz.m3u8"
  },
  {
    "id": generateId(),"vote": 2, "title": "Pruduct Hunt", "link": "https://test-streams.mux.dev/x36xhzz/x36xhzz.m3u8"
  },
  {
    "id": generateId(),"vote": 3, "title": "Stackoverflow", "link": "https://d2zihajmogu5jn.cloudfront.net/bipbop-advanced/bipbop_16x9_variant.m3u8"
  },
  {
    "id": generateId(),"vote": 8, "title": "Reddit", "link": "https://test-streams.mux.dev/x36xhzz/x36xhzz.m3u8"
  },
  {
    "id": generateId(),"vote": 4, "title": "Github", "link": "https://test-streams.mux.dev/x36xhzz/x36xhzz.m3u8"
  }
])



CacheService.set(CACHE_KEYS.DATA, initalData)

const root = document.querySelector('#root')
function renderApp (hash: string) {
  root.classList.remove('fade-in')
  let app: Add | List;
  root.innerHTML = ''
  if (hash === '#new') {
    app = new Add()
  } else {
    app = new List()
  }

  app.render(root)
  setTimeout(() => {
    root.classList.add('fade-in')
  }, 300);
}

window.addEventListener('load', () => {
  renderApp(window.location.hash)
})

window.onhashchange = function (e: any) {
  renderApp(e.currentTarget.location.hash)
}