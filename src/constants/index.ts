export const CACHE_KEYS = {
  DATA: 'DATA'
}

export const ICONS = {
  DONE: `<i class="material-icons done">done</i>`,
  ARROW_BACK: `<i class="material-icons">arrow_back</i>`,
  ARROW_NEXT: `<i class="material-icons">arrow_forward</i>`,
  ADD: `<i class="material-icons">add</i>`,
  REMOVE: `<i class="material-icons">remove</i>`,
  PLAY: '<i class="material-icons">play_circle</i>'
}