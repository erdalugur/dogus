import { CACHE_KEYS, ICONS } from "../constants"
import { CacheService, showMessage, replaceHash, generateId } from "../lib"
import { ItemType } from "../types"

export class Add {

  el: Element
  form: Element
  canSubmit: boolean = true

  render (el: Element) {
    this.el = el
    el.innerHTML = `
    <div class="row add-new">
      <div class="form-container">
        <div class="card-list">
        <div class="add-new">
          <span class="return"> 
            ${ICONS.ARROW_BACK}
            Return to list
          </span>
        </div>
      </div>
      </div>
    </div>
    `
    el.querySelector('.return').addEventListener('click', () => replaceHash(''))
    this.renderForm()
  }

  private renderForm () {
    const form = document.createElement('form')
    form.onsubmit = this.submit

    form.innerHTML = `
      <h3>Add New Link</h3>
      <div class="form-group">
        <label>Title</label>
        <input class="title"/>
        <label class="error">This field is required</label>
      </div>
      <div class="form-group">
        <label>Url</label>
        <input class="url"/>
        <label class="error">This field is required</label>
      </div>
     <div>
      <button class="btn-dark rounded btn-add">Add</button>
     </div>
    `
    this.el.appendChild(form)
    this.form = form
  }

  private submit = (e: SubmitEvent) => {
    e.preventDefault()
    if (!this.canSubmit) return

    const errros: string [] = []
    const title = document.querySelector('.title') as HTMLInputElement
    const url = document.querySelector('.url') as HTMLInputElement

    if (title.value.length === 0) {
      title.nextElementSibling.setAttribute('style', 'display: block');
      errros.push('title')
    } else {
      title.nextElementSibling.setAttribute('style', 'display: none');
    }

    if (url.value.length === 0) {
      url.nextElementSibling.setAttribute('style', 'display: block');
      errros.push('url')
    } else {
      url.nextElementSibling.setAttribute('style', 'display: none');
    } 
    if (errros.length > 0 ) return

    const items: ItemType[] = CacheService.get(CACHE_KEYS.DATA, [])

    const item: ItemType = {
      title: title.value,
      vote: 0,
      link: url.value,
      id: generateId()
    }

    items.unshift(item)
    CacheService.set(CACHE_KEYS.DATA, items)
    this.canSubmit = true
    showMessage('Process successfully completed')
    replaceHash('')
  } 
}