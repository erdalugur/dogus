import { CACHE_KEYS, ICONS } from '../constants'
import { VideoJS, VideoJsPlayerOptions, VideoJsPlayer, showConfirm, CacheService, showMessage, replaceHash } from '../lib'
import { ItemType } from '../types'

export class List {

  activePlayer: VideoJsPlayer

  items: ItemType[] = []

  sorting: 'asc' | 'desc' = 'asc'

  el: Element

  render (el: Element) {
    el.innerHTML = `
    <div class="row">
      <div class="form-container">
        <div class="card-list">
        <div class="add-new">
          <button class="add"> 
            ${ICONS.ADD}
            SUBMIT A LINK
          </button>
        </div>
        <div class="filters"></div>
        <div class="items"></div>
        <div class="pagination"></div>
      </div>
      </div>
      <div class="player-wrapper">
        <div 
          id="player-container"
          class="video-player"
          controls 
          preload="auto"
        ></div>
      </div>
    </div>
    `
    this.el = el
    this.loadItems()

  }

  async loadItems () {
    this.items = CacheService.get<ItemType[]>(CACHE_KEYS.DATA, [])
    this.renderItems()
    this.renderSorting()
    this.bindActions()
  }

  bindActions () {
    document.querySelector('.add').addEventListener('click', () => replaceHash('new'))
  }

  renderItem (item: ItemType) {
    return `<div class="card" data-item='${item.id}'>
    <div class="point" data-action="play_circle">
      <span class="vote">${item.vote}</span>
      <label class="">Point</label>
      ${ICONS.PLAY}
    </div>
    <div class="content">
      <div class="card-header">
        <strong>${item.title}</strong>
        <label>(${item.link})</label>
      </div>
      <div class="remove"><span class="material-icons">do_disturb_on</span></div>
      <div class="votes">
        <label data-action="up" data-id="${item.id}">
          <span class="material-icons">
            arrow_upward
          </span>
          Up Vote
        </label>
        <label data-action="down" data-id="${item.id}"> 
          <span class="material-icons">
            arrow_downward
          </span>
          Down Vote
        </label>
      </div>  
    </div>
  </div>
  `
  }

  pageNumber: number = 1
  pageLimit: number = 5
  
  totalPages = () => Math.ceil(this.items.length / this.pageLimit)

  renderItems () {
    const offset = (this.pageNumber - 1) * this.pageLimit
    const items = this.items.sort((a,b) => {
      if (this.sorting === 'asc') 
        return  a.vote - b.vote

      return b.vote - a.vote
    }).slice(offset, offset + this.pageLimit)
    const itemsEl = document.querySelector('.items')
    itemsEl.innerHTML = ''
    for (let index = 0; index <items.length; index++) {
      const item = items[index]
      itemsEl.innerHTML += this.renderItem(item)
    }

    this.renderPagination()

    this.initPlayer(items[0])

    document.querySelectorAll('.card .remove').forEach(item => {
      item.addEventListener('click', () => {
        const data = this.getActiveData(item.parentElement.parentElement)
        showConfirm(data.title, () => {
          this.items = this.items.filter(x => x.id !== data.id)
          CacheService.set(CACHE_KEYS.DATA, this.items)
          this.renderItems()
          this.renderSorting()
          showMessage('Process successfully completed')
        })
      })
    })

    document.querySelectorAll('.votes label').forEach(item => {
      item.addEventListener('click', () => {
        const action = item.getAttribute('data-action') as 'up' | 'down'
        const id = item.getAttribute('data-id')
        const index = this.items.findIndex(x => x.id === id)
        if (index > -1) {
          this.items[index].vote = (action === 'up' ? this.items[index].vote + 1 : this.items[index].vote -1)
        }
        this.renderItems()
      })
    })

    document.querySelectorAll('.point').forEach(item => {
      item.addEventListener('click', () => {
               
        item.parentElement.classList.add('active')
        const action = item.getAttribute('data-action') as 'play_circle' | 'pause'
        if (action === 'play_circle') {
          const video = this.activePlayer.el().parentElement
          if (item.parentElement.getAttribute('data-item') !== video.getAttribute('data-id')) {
            const data = this.getActiveData(item.parentElement)
            this.initPlayer(data)
          }
          this.activePlayer.play()
        } else {
          this.activePlayer.pause()
        }
        const newAction = action === 'play_circle' ? 'pause' : 'play_circle'
        item.querySelector('.material-icons').innerHTML = newAction
        item.setAttribute('data-action', newAction)

        Array.from(document.querySelectorAll('.point')).filter(x => x !== item).forEach(x=> {
          x.parentElement.classList.remove('active')
          x.querySelector('.material-icons').innerHTML = 'play_circle'
          x.setAttribute('data-action', 'play_circle')
        })
      })
    })
  }

  getActiveData (el: HTMLElement) {
    const id = el.getAttribute('data-item')
    return this.items.find(x => x.id.toString() === id)
  }

  renderSorting (){
    const createOption = (label: string, value: string, selected: boolean = false) => {
      return `<option value="${value}" ${selected ? "selected" : ""}>${label}</option>`
    }

    const items = [
      { value: 'asc', label: 'Order By' },
      { value: 'desc', label: 'Most Voted (Z-A)' },
      { value: 'asc', label: 'Less Voted (A-Z)' }
    ]

    const el = document.querySelector('.filters')
    el.innerHTML = this.items.length === 0 ? '' : `
    <select>
    ${ items.map(x => createOption(x.label, x.value, x.value === this.sorting))}
    </select>`
    
    const select = document.querySelector('.filters select')
    const self = this
    select?.addEventListener('change', function () {
      self.sorting = this.value as 'asc' | 'desc'
      self.renderItems()
    })
  }

  renderPagination () {
    const pages = Array.from({ length: this.totalPages() }).map((x, i) => i + 1)
    const el = document.querySelector('.pagination')
    el.innerHTML = this.items.length > 0 ? `
    <a href="javascript:;" class="arrow" data-action="prev">
      ${ICONS.ARROW_BACK}
    </a>
    ${pages.map(page => `<a class="pager ${this.pageNumber === page ? 'active': ''}" data-value="${page}" href="javascript:;">${page}</a>`).join('')}
    <a href="javascript:;" class="arrow" data-action="next">
      ${ICONS.ARROW_NEXT}
    </a>
  ` : ''

  document.querySelectorAll('.pager').forEach(a => {
    a.addEventListener('click', () => {
      this.pageNumber = parseInt(a.getAttribute('data-value'))
      this.renderItems()
    })
  })

  el.querySelectorAll('.arrow').forEach(a => {
    a.addEventListener('click', () => {
      const action = a.getAttribute('data-action') as 'prev' | 'next'
      const number = action === 'next' ? this.pageNumber + 1 : this.pageNumber -1
      if (number > this.totalPages() || number === 0) return

      this.pageNumber = number
      this.renderItems()
    })
  })
  }

  initPlayer (item: ItemType) {
    if (this.activePlayer) {
      this.activePlayer.dispose()
    }

    if (this.items.length === 0) return
    
    const options: VideoJsPlayerOptions = {
      id: '#player-container',
      sources: [{
          src: item.link,
          type: item.link.includes('.m3u8') ? 'application/x-mpegURL' : 'video/mp4'
      }],
      autoplay: false,
      dataId: item.id.toString()
    }
    this.activePlayer = new VideoJS(options).createInstance()

    this.activePlayer.on('pause', function() {
      const item = this.el_ as HTMLElement
      const id = item.parentElement.getAttribute('data-id')
      const el = document.querySelector(`[data-item="${id}"]`);
      el.querySelector('.material-icons').innerHTML = 'play_circle'
      el.querySelector('.point').setAttribute('data-action', 'play_circle')
    })

    this.activePlayer.on('play', function () {
      const item = this.el_ as HTMLElement
      const id = item.parentElement.getAttribute('data-id')
      const el = document.querySelector(`[data-item="${id}"]`);
      el.querySelector('.material-icons').innerHTML = 'pause'
      el.classList.add('active')
      el.querySelector('.point').setAttribute('data-action', 'pause')
    })
  }
}
