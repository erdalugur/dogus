import videojs, { VideoJsPlayer, VideoJsPlayerOptions as Options} from 'video.js'

interface VideoJsPlayerOptions extends Options {
  id: string
  dataId: string
}

class VideoJS {
  el: Element
  id: string
  options: VideoJsPlayerOptions
  constructor(options: VideoJsPlayerOptions) {
    this.options = options;
    this.id = `player-${new Date().getTime().toString()}`
    this.el = document.querySelector(options.id)
    this.el.setAttribute('data-id', options.dataId)
  }

  createInstance () {
    const { width, height, sources, language, preload, autoplay } = this.options
    const video = document.createElement('video')
    video.id = this.id
    video.className = 'video-js vjs-default-skin vjs-big-play-centered';
    video.setAttribute('controls', '');
    video.setAttribute('preload', preload || '');
    video.setAttribute('language', language);
    if (autoplay)
      video.setAttribute('autoplay', "")
      
    if (height){
      video.style.height = `${height}px`
    }
    if (width) {
      video.style.width = `${width}px`
    }

    for (let index = 0; index < sources.length; index++) {
      const source = sources[index];
      var sourceEl = document.createElement('source');
      sourceEl.setAttribute('src', source.src)
      sourceEl.setAttribute('type', source.type);
      video.appendChild(sourceEl)
    }

    this.el.appendChild(video)

    const options: any = {
      aspectRatio: '4:3'
    }

    if (this.options.controlBar) {
      options.controlBar = this.options.controlBar
    }
    options.controlBar = Object.assign({}, options.controlBar, {
      pictureInPictureToggle: false,
        volumePanel: {
          inline: false
        }
    })
    if (this.options.plugins) {
      for (let index = 0; index < this.options.plugins.length; index++) {
        const plugin = this.options.plugins[index];
        videojs.registerPlugin(plugin.name, plugin)
      }
    }

    return videojs(this.id, {...options})
  }
}

export {
  VideoJsPlayerOptions,
  VideoJS,
  VideoJsPlayer
}