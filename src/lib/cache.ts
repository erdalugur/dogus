export class CacheService {
  static get <T>(key: string, initialData: T): T {
    try {
      const value = window.localStorage.getItem(key)
      return value ? (JSON.parse(value) as T) : initialData
    } catch (error) {
      return initialData
    }
  }
  
  static set <T>(key: string, value: T) {
    window.localStorage.setItem(key, JSON.stringify(value))
  }

  static remove (key: string) {
    window.localStorage.removeItem(key)
  }

  static clear () {
    window.localStorage.clear()
  }
}
