import { ICONS } from "../constants"

function removeWithAnimation (target: HTMLElement) {
  if (!target) return
  target.style.animation = 'hide 0.5s ease-in-out forwards'
  setTimeout(() => {
    document.body.removeChild(target)
  }, 500);
}

export function showConfirm (message: string, fn: () => void) {

  const target = document.body

  function remove () {
    removeWithAnimation(document.querySelector('#confirm'))
  }

  if (target.querySelector('#confirm')) {
    remove()
  }
  const confirm = document.createElement('div')
  confirm.setAttribute('id', 'confirm')
  confirm.innerHTML = `
  <div class="modal">
    <div class="modal-content">
      <div class="modal-header"> 
        <div>Remove Link</div>
      </div>
      <div class="modal-body">
        <h3>Do you want to remove?</h3>
        <p>${message}</p>
      </div>
      <div class="modal-footer">
        <button id="ok" class="rounded btn-dark">OK</button>
        <button id="cancel" class="rounded btn-dark">CANCEL</button>
      </div>
    </div>
  </div>
  `
  target.appendChild(confirm)

  target.querySelector('.modal-footer > #ok').addEventListener('click', () => {
    fn()
    remove()
  })
  target.querySelector('.modal-footer > #cancel').addEventListener('click', remove)
}


export function showMessage (message: string, duration: number = 3000) {
  const target = document.body

  function remove () {
    removeWithAnimation(document.querySelector('#message-box'))
  }

  if (target.querySelector('#message-box')) {
    remove()
  }
  const confirm = document.createElement('div')
  confirm.setAttribute('id', 'message-box')
  confirm.innerHTML = `
  <div class="modal">
    <div class="modal-content">
      <div class="modal-body" style="padding-top: 20px;">
      ${ICONS.DONE}
        <h3>${message}</h3>
      </div>
      <div class="modal-footer">
        <button id="ok" class="rounded btn-dark">OK</button>
      </div>
    </div>
  </div>
  `
  setTimeout(() => {
    target.appendChild(confirm)
    target.querySelector('#message-box').querySelector('#ok').addEventListener('click', () => remove())

  }, 500);

  setTimeout(() => {
    remove()
  }, duration);

}