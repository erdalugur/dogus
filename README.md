`Study Case`


```js
⚡ Projede Kullanılan Bileşenler
```
- Typescript
- ES6+
- Video.js
- Sass
- Webpack
<br>


```js
👉 Proje scripleri
```
```js
// Buradaki scriptler package.json da bulunur.

// Development command
npm run dev

// Production build command
npm run build

// Serving
npm run start
```

<br />

```js
👉 Projeyi Docker ile çalıştırmak
```


Bilgisayarınızda Docker kurulu olmalı. Henüz kurulu değilse  [buradan](https://docs.docker.com/desktop/) indirip kurabilirsiniz.
Ardından aşağıdaki adımları izleyip projeyi debug edebilirsiniz.
```js
// proje root klasöründe aşağıdaki komutu çalıştırın.

docker-compose up
```

```js
👉 Local ortamda Projeyi Çalıştırmak
```

Bilgisayarınızda Node.js kurulu olmalı. Henüz kurulu değilse  [buradan](https://nodejs.org/en/download/) indirip kurabilirsiniz.
Ardından aşağıdaki adımları izleyip projeyi debug edebilirsiniz.

```js
// aşağıdaki komut ile global olarak serve paketini indirin.
npm install -g serve

// projeyi build edin
npm run build

// ardından bu komutu proje root klasöründe çalıştırın
npm run start
```

<br>
<p>👍 Bu adımlardan sonra proje <a target="http://localhost:3000" target="_blank">burada</a> hizmet veriyor olmalıdır.</p>
<br>

```js
💡 Notlar
```

- Bilgisayarınızda 3000 portunda herhangi bir uygulama çalışıyorsa bu uygulamanın portunu değiştirebilir yada çalışan diğer uygulamayı durdurabilirsiniz.
- Uygulama liste ve ekleme modulü içeriyor
- Uygulama açılırken default olarak liste modülü 5 kayıtla açılıyor istenirse eklenebilir veya silinebilir.
- Link ekle modulü taryıcının hash'ine subscribe olmuş durumda ve hash #new olduğunda görüntülenir. 
- Video url'i eklenirken .mp4 veya .m3u8 uzantılı url eklenmelidir aksi halde video oynatılmaz.
- Sayfalama her 5 elemanda 1 artacaktır
- Sıralamalar vote değerine göre değiştirilebilir.
- Web ve mobile görünümleri için css yazılmıştır diğer ara çözünürlükler görmezden gelinmiştir.
- Stillendirme yaparken herhangi 3. parti kütüphane veya framework kullanılmamıştır.
- Play list'in ilk elemanı default olarak oynatılabilir. Istenirse herhangi bir eleman oynatılabilir(Vote değerinin üzerine gelince play iconu belirecektir).
- Up ve down vote düğmeleri ile ilgili elemanın 'vote' değeri güncellenebilir. Değer değiştikçe sıralama otomatik olarak değişecektir.
- Tüm kayıtlar localstorage'da tutuluyor.
<br>

`Uğur Erdal | Senior Frontend Developer`

<a href="https://www.linkedin.com/in/ugur-erdal/" target="_blank">👉  Linkendin</a>

<a href="https://github.com/erdalugur" target="_blank">👉  Github</a>

<a href="mailto:info@ugurerdal.com" target="_blank">👉  E-Posta</a>
