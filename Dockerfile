FROM node:14-slim
WORKDIR .
COPY . .
RUN npm install -g serve
RUN npm install
RUN npm run build
EXPOSE 3000
CMD ["npm", "run", "start"]
