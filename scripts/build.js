const configFactory = require('../webpack.config')
const webpack = require('webpack')
const config = configFactory('production')
const compiler = webpack(config)

function build () {
  compiler.run((err, result) => {
    console.log()
    console.log()
    if(err ) {
      console.log(JSON.stringify(err, null, 2))
      return
    }
  
    const stats = result.toJson({ errors: true, warnings: true })
    const errors = (stats.errors || []).map(x => x.message)
    if (errors.length > 0) {
      console.log(JSON.stringify(errors, null, 2))
      return
    }
    console.log('Build successfully...')
    console.log()
    process.exit(0)
  })
}

build()