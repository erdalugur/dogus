const configFactory = require('../webpack.config')
const webpack = require('webpack')
const webpackDevServer = require('webpack-dev-server')

const config = configFactory('development')
const compiler = webpack(config)

const server = new webpackDevServer(config.devServer, compiler)

async function runServer () {
  await server.start()
}

runServer();