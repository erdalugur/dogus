const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const { CleanWebpackPlugin } = require('clean-webpack-plugin'); // installed via npm
const webpack = require('webpack')

const paths = {
  appIndexJs: path.join(__dirname, 'src/index.ts'),
  appBuild: path.resolve(__dirname, './build'),
  appIndexHtml: path.join(__dirname, 'public/index.html'),
  appPublic: path.join(__dirname, 'public')
}

/**
 * 
 * @param {'production' | 'development' } mode 
 * @returns 
 */
const configFactory = (mode) => {
  return {
    mode: mode,
    entry: paths.appIndexJs,
    output: {
      path: paths.appBuild,
      filename: 'bundle.[contenthash:8].js',
    },
    devtool: 'source-map',
    target: ["web", "es5"],
    resolve: {
      extensions: ['.ts', '.tsx', '.js', '.scss', '.css']
    },
    module: {
      rules: [
        {
          test: /\.(sa|sc|c)ss$/,
          include: [
            path.resolve(__dirname, 'src', 'styles')
          ],
          use: [
            MiniCssExtractPlugin.loader,
            {
              loader: 'css-loader',
              options: { 
                url: false,
                sourceMap: false,
              }
            },
            'sass-loader',
          ]
        },
        {
          test: /\.ts?$/,
          use: 'ts-loader',
          exclude: /node_modules/,
        },
      ]
    },  
    plugins: [
      new webpack.ProgressPlugin(),
      new CleanWebpackPlugin(),
      new HtmlWebpackPlugin({
        template: paths.appIndexHtml,
        inject: 'body'
      }),
      new MiniCssExtractPlugin({
        filename: '[name].[contenthash:8].css'
      })
    ],
    watch: true,
    devServer: {
      client: {
        progress: true,
      },
      static: {
        directory: paths.appBuild,
      },
      compress: true,
      port: 3000,
      open: true,
      devMiddleware: {
        publicPath: paths.appBuild,
        writeToDisk: true,
      },
    },
    stats: 'errors-only'
  }
}

module.exports = configFactory